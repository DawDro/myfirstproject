# Simulated election

import random

trials = 10_000

regions = 3

def choose_candidate(probability):
    if random.random() < probability:
        return "candidate A"
    else:
        return "candidate B"

candidateA_wins = 0
candidateB_wins = 0

for region in range(regions):
    candidateA_votes = 0
    candidateB_votes = 0
    
    if region == 0:
        prob_ratio = .87
    elif region == 1:
        prob_ratio = .65
    else:
        prob_ratio = .17
 
    for trial in range(trials):
        if choose_candidate(prob_ratio) == "candidate A":
            candidateA_votes = candidateA_votes + 1
        else:
            candidateB_votes = candidateB_votes + 1
    
    if candidateA_votes > candidateB_votes:
        candidateA_wins += 1
    else:
        candidateB_wins += 1

    print(f"Region {region} votes for candidate A: {candidateA_votes}, B: {candidateB_votes}")
print(f"Candidate A wins in {candidateA_wins} regions and B wins in {candidateB_wins}.")
if candidateA_wins > candidateB_wins:
    print(f"Candidate A wins the Elections!")
else:
    print(f"Candidate B wins the Elections!")
