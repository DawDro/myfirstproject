# 1. Using Tkinter from IDLE’s interactive window, execute code that creates a window with a Label widget displaying the text "GUIs are great!" 
# 2. Repeat exercise 1 with the text "Python rocks!" 
# 3. Repeat exercise 1 with the text "Engage!"



import tkinter as tk

window = tk.Tk()

greeting = tk.Label(text="GUIs are great!") # Just change text to exercise 2 and 3...
greeting.pack()

window.mainloop()