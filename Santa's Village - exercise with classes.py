class SantaVillage:
    name = "Santa's Village"
    
    def __init__(self, population, area):
        self.population = population
        self.area = area

    def present_village(self):
        return f"Welcome in {self.name}. The village has {self.population} residents and {self.area} sqm area."

    def add_residents(self, number_of_new_residents):
        self.population += number_of_new_residents

    def reduce_population(self, reduced_number):
        self.population -= reduced_number

    def extend_village_area(self, sqm_to_add):
        self.area += sqm_to_add

# Tests for SantaVillage class
v1 = SantaVillage(50,800)
print(v1.present_village())
v1.add_residents(20)
print(v1.present_village())
v1.reduce_population(5)
print(v1.present_village())
v1.extend_village_area(500)
print(v1.present_village())

class Street():
    def __init__(self, name, length, number_of_lanterns, buildings_quantity, buildings_numbers):
        self.name = name
        self.length = length
        self.number_of_lanterns = number_of_lanterns
        self.buildings_numbers = buildings_numbers
        self.buildings_quantity = len(buildings_numbers) +1

    def add_lantern(self, number_of_new_lanterns):
        self.number_of_lanterns += number_of_new_lanterns

    def which_village(self):
        return super().name

    def add_building_number(self, new_number):
        self.buildings_numbers.append(new_number)

# Tests for Street class
s1 = Street("Elfs st.", 30, 6, 2,[1,2])
print(s1.name)
print(s1.buildings_numbers)
print(s1.which_village())
s1.add_building_number(3)
print(f"Street's building numbers: {s1.buildings_numbers}. Quantity of buildings on the street: {s1.buildings_quantity}")

        
class Building():
    def __init__(self, number, area, floors, has_shops):
        self.number = number
        self.area = area
        self.floors = floors
        self.has_shops = has_shops # t/f

    def describe_building(self):
        return f"The building number {self.number} has {self.area} sqm and {self.floors} floors. The building {self.shops()}."

    def shops(self):
        if self.has_shops == True:
            return "has shops"
        return "doesn't have shops"

# Tests for Building class
b1 = Building(3,300,3,False)
print(b1.describe_building())
print(f"The building is in {b1.which_village()}")

class Flat:
    def __init__(self, building_number, street, floor,flat_number, area, no_of_rooms):
        self.building_number = building_number
        self.street = street
        self.floor = floor
        self.flat_number = flat_number
        self.area = area
        self.no_of_rooms = no_of_rooms

    def locate_flat(self):
        return f"The flat is in {self.building_number} building by {self.street} on {self.floor} floor under number {self.flat_number}."

# Tests for Flat class
f1 = Flat(3, "Elfs st.", 2, 31)
print(f1.locate_flat())
