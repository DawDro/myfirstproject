# 1. Write a program that grabs the full HTML from the web page at http://olympus.realpython.org/profiles/dionysus.

from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles/dionysus"

page = urlopen(url)

html_bytes = page.read()

html = html_bytes.decode("utf-8")

print(html)

# 2 .Use the string .find() method to display the text following “Name:” and “Favorite Color:” 
# (not including any leading spaces or trailing HTML tags that might appear on the same line). 

start_string = "Name: "
end_string = "</h2>"

def find_string(start_str, end_str):
    name_start_index = html.find(start_str) + len(start_str)
    name_end_index = html.find(end_str)
    string = html[name_start_index:name_end_index]
    return string

print(find_string(start_string,end_string))
start_string = "Favorite Color: "
end_string = "\n</center>"
print(find_string(start_string,end_string))

# 3. Repeat the previous exercise using regular expressions. The end of each pattern should be a "<" (the start of an HTML tag)
#  or a newline character, and you should remove any extra spaces or newline characters from the resulting text using the string .strip() method.

import re

pattern1 = "Name: .*?<"
pattern2 = "Favorite Color: .*?\n<"
def find_string_pattern(pattern,html):
    match_results = re.search(pattern, html, re.IGNORECASE)
    string = match_results.group()
    string = re.sub(".*:.","",string)
    string = re.sub("<","",string)
    return string
    
print(find_string_pattern(pattern1,html))
print(find_string_pattern(pattern2,html))