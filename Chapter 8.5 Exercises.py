#1

for n in range(5):
    str = input("Please give any sign to print. Use 'q' or 'Q' to quit.")
    if str == "Q" or str == "q":
        print("Program finished")
        break
    else:
        print(str)

#2
for n in range(1,50):
    if n % 3 == 0:
        continue
    else:
        print(n)
