# Example of packing 3 label widgets into frame (by default it goes form up to down one under another)
import tkinter as tk

window = tk.Tk()

frame1 = tk.Frame(master=window,width=100,height=100,bg="red")
frame1.pack()

frame2 = tk.Frame(master=window,width=50,height=50,bg="yellow")
frame2.pack()

frame3 = tk.Frame(master=window,width=25,height=25,bg="blue")
frame3.pack()

window.mainloop()

# Example with using directions axis ( Now width is not needed because the frame fills whole X axis.) - used when window needs to be resized etc.
# tk.X fills in the horizontal direction. tk.Y fills vertically. tk.BOTH fills in both directions.

window2 = tk.Tk()

frame1 = tk.Frame(master=window2,width=100,height=100,bg="red")

frame2 = tk.Frame(master=window2,width=50,height=50,bg="yellow")

frame3 = tk.Frame(master=window2,width=25,height=25,bg="blue")

frame1.pack(fill=tk.X)
frame2.pack(fill=tk.X)
frame3.pack(fill=tk.X)

window2.mainloop()

# The arguments which specifies on which side of window the frame should be placed
# tk.TOP, tk.BOTTOM, tk.LEFT, and tk.RIGHT.

window3 = tk.Tk()

frame1 = tk.Frame(master=window3,width=200,height=100,bg="red")
frame1.pack(fill=tk.Y,side=tk.LEFT)

frame2 = tk.Frame(master=window3,width=100,bg="yellow")
frame2.pack(fill=tk.Y,side=tk.LEFT)

frame3 = tk.Frame(master=window3,width=50,bg="blue")
frame3.pack(fill=tk.Y,side=tk.LEFT)

window3.mainloop()

# Window with default set of size but responsively resized in both ways
window4 = tk.Tk()

frame1 = tk.Frame(master=window4,width=200,height=100,bg="red")
frame1.pack(fill=tk.BOTH,side=tk.LEFT,expand=True)

frame2 = tk.Frame(master=window4,width=100,bg="yellow")
frame2.pack(fill=tk.BOTH,side=tk.LEFT,expand=True)

frame3 = tk.Frame(master=window4,width=50,bg="blue")
frame3.pack(fill=tk.BOTH,side=tk.LEFT,expand=True)

window4.mainloop()

# The .place() Geometry Manager. You must provide two keyword arguments, x and y, that specify the x- and y-coordinates for the top-left corner of the widget. 
# Both x and y are measured in pixels, not text units.

window5 = tk.Tk()

frame0 = tk.Frame(master=window5,width=150,height=150,bg="green")
frame0.pack()

label1 = tk.Label(master=frame0,text="I'm at(0,0)",bg="blue")
label1.place(x=0,y=0)

label2 = tk.Label(master=frame0,text="I'm at(75,75)",bg="yellow")
label2.place(x=75,y=75)

window5.mainloop()
# Explanation above: The frame is placed in window and there are 2 labels into frame.

# The .grid() Geometry Manager.
# .grid() works by splitting a window or Frame into rows and columns. You specify the location of a widget by calling .grid() and passing the row and column indices to the row and column keyword arguments, respectively.

window6 = tk.Tk()

for i in range(3):
    # Dynamic expanding added below:
    window6.columnconfigure(i,weight=1,minsize=75)
    window6.rowconfigure(i, weight=1,minsize=50)

    for j in range(3):
        frame6 = tk.Frame(
            master=window6,
            relief=tk.RAISED,
            borderwidth=1
        )
        frame6.grid(row=i,column=j,padx=5,pady=5)
        label6 = tk.Label(master=frame6,text=f"Row {i}\nColumn {j}")
        label6.pack(padx=5,pady=5)

window6.mainloop()

# Sticky labels, N, W, S, E - directions to stick text inside labels, can be combined as directions ne, sw, etc.

window7 = tk.Tk()

window7.columnconfigure(0,minsize=250)
window7.rowconfigure([0,1],minsize=100)

label7 = tk.Label(text="A")
label7.grid(row=0,column=0,sticky="ne")

label8 = tk.Label(text="B")
label8.grid(row=1,column=0,sticky="sw")

window7.mainloop()

# Experimenting with sticky

window8 = tk.Tk()

window8.rowconfigure(0,minsize=50)
window8.columnconfigure([0,1,2,3],minsize=50)


label9 = tk.Label(text="1", bg="black", fg="white")
label10 = tk.Label(text="2", bg="black", fg="white")
label11 = tk.Label(text="3", bg="black", fg="white")
label12 = tk.Label(text="4", bg="black", fg="white")

label9.grid(row=0, column=0)
label10.grid(row=0, column=1, sticky="ew")
label11.grid(row=0, column=2, sticky="ns")
label12.grid(row=0, column=3, sticky="nsew")

window8.mainloop()


