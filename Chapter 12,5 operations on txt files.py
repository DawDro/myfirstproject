Python 3.12.1 (tags/v3.12.1:2305ca5, Dec  7 2023, 22:03:25) [MSC v.1937 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
from pathlib import Path
path = Path.home() / "hello.txt"
path.touch()
file = path.open(mode="r", encoding="utf-8")
file
<_io.TextIOWrapper name='C:\\Users\\dawdr\\hello.txt' mode='r' encoding='utf-8'>
file.close()
file
<_io.TextIOWrapper name='C:\\Users\\dawdr\\hello.txt' mode='r' encoding='utf-8'>
file_path = "C:/Users/dawdr/hello.txt"
file = open(file_path,more="r",ecoding="utf-8")
Traceback (most recent call last):
  File "<pyshell#8>", line 1, in <module>
    file = open(file_path,more="r",ecoding="utf-8")
TypeError: 'more' is an invalid keyword argument for open()
file = open(file_path,mode="r",ecoding="utf-8")
Traceback (most recent call last):
  File "<pyshell#9>", line 1, in <module>
    file = open(file_path,mode="r",ecoding="utf-8")
TypeError: 'ecoding' is an invalid keyword argument for open()
file = open(file_path,mode="r",encoding="utf-8")
file
<_io.TextIOWrapper name='C:/Users/dawdr/hello.txt' mode='r' encoding='utf-8'>
file.close()
path = Path.home() / "hello.txt"
with path.open(mode="r",encoding="utf-8") as file:
    text = file.read()

    
with path.open(mode="r",encoding="utf-8") as file:
    text = file.read()

    
text
'Hello World.'
text
'Hello World.'
with path.open(mode="r",encoding="utf-8") as file:
    text = file.read()

    
text
'Hello World.\nSecond Hello.'
with path.open(mode="r",encoding="utf-8") as file:
    for line in file.readlines():
        print(line)

        
Hello World.

Second Hello.
with path.open(mode="r",encoding="utf-8") as file:
    for line in file.readlines():
        print(line,end="")

        
Hello World.
Second Hello.
with path.open(mode="r",encoding="utf-8") as file:
    file.write("Hello there!")

    
Traceback (most recent call last):
  File "<pyshell#31>", line 2, in <module>
    file.write("Hello there!")
io.UnsupportedOperation: not writable
with path.open(mode="w",encoding="utf-8") as file:
    file.write("Hello there!")

    
12
file
<_io.TextIOWrapper name='C:\\Users\\dawdr\\hello.txt' mode='w' encoding='utf-8'>
>>> print(file)
<_io.TextIOWrapper name='C:\\Users\\dawdr\\hello.txt' mode='w' encoding='utf-8'>
>>> with path.open(mode="r",encoding="utf-8") as file:
...     for line in file.readlines():
...         print(line,end="")
... 
...         
Hello there!
>>> with path.open(mode="a",encoding="utf-8") as file:
...     file.write("\nHello")
... 
...     
6
>>> file
<_io.TextIOWrapper name='C:\\Users\\dawdr\\hello.txt' mode='a' encoding='utf-8'>
>>> with path.open(mode="r",encoding="utf-8") as file:
...     for line in file.readlines():
...         print(line,end="")
... 
...         
Hello there!
Hello
>>> lines_of_text = [
...     "Hello line 1\n",
...     "Hello line 2\n",
...     "Hello line 3\n"]
>>> with path.open(mode="w",encoding="utf-8") as file:
...     file.writelines(lines_of_text)
... 
...     
>>> with path.open(mode="r",encoding="utf-8") as file:
...     for line in file.readlines():
...         print(line,end="")
... 
...         
Hello line 1
Hello line 2
Hello line 3
