
def cats_with_hats(cats_array):
    cats_with_hats_on = []

    for num in range(1,101):
        for cat in range(1,101):
            if cat % num == 0:
                if cats_array[cat] is True:
                    cats_array[cat] = False
                else:
                    cats_array[cat] = True

    for cat in range(1,101):
        if cats_array[cat] is True:
            cats_with_hats_on.append(cat)

    return cats_with_hats_on

cats = [False] * 101
print(cats_with_hats(cats))
