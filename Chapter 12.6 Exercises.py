from pathlib import Path
import csv

# 1
file_path = Path.home() / "Desktop" / "Python" / "Python Basics" / "numbers.csv"

numbers = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
]

with file_path.open(mode="w",encoding="utf-8",newline="") as file:
    writer = csv.writer(file)
    for number in numbers:
        writer.writerow(number)


# 2
int_numbers = []
with file_path.open(mode="r",encoding="utf-8",newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        int_row = [int(value) for value in row]
        int_numbers.append(int_row)

#print(int_numbers)

# 3

colours_file_path = Path.home() / "Desktop" / "Python" / "Python Basics" / "favorite_colorus.csv"

favorite_colors = [
    {"name": "Joe", "favorite_color": "blue"},
    {"name": "Anne", "favorite_color": "green"},
    {"name": "Bailey", "favorite_color": "red"},
]

with colours_file_path.open(mode="w",encoding="utf-8",newline="") as file2:
    writer = csv.DictWriter(file2, fieldnames=["name", "favorite_color"])
    writer.writeheader()
    writer.writerows(favorite_colors)
    file.close()

# 4
with colours_file_path.open(mode="r",encoding="utf-8",newline="") as file2:
    reader = csv.DictReader(file2)
    for row in reader:
        print(row)
