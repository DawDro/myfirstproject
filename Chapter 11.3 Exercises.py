from pathlib import Path

# 1
my_dir = Path.home() / "Desktop" / "Python" / "Python Basics" / "Exercise 11.3"
print(my_dir.exists())

my_dir.mkdir(exist_ok=True)
print(my_dir.exists())

# 2

f1 = my_dir / "file1.txt"
f2 = my_dir / "file2.txt"
f3 = my_dir / "image1.png"

f1.touch()
f2.touch()
f3.touch()

# 3
img_dir = my_dir / "images"
img_dir.mkdir(exist_ok=True)

f3.replace(img_dir / "image1.png")

# 4

f1.unlink()

# 5

import shutil
shutil.rmtree(img_dir)
