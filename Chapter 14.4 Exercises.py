from PyPDF2 import PdfMerger
from pathlib import Path

pdf_merger = PdfMerger()

reports_dir = (Path.home() / "Desktop" / "Python" / "Python Basics" )

#1

m1 = reports_dir / "merge1.pdf"
m2 = reports_dir / "merge2.pdf"

pdf_merger.append(str(m1))
pdf_merger.append(str(m2))

with Path("concatenated.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)

# 2
    
pdf_merger2 = PdfMerger()

m3 = reports_dir / "merge3.pdf"
conc_file = reports_dir / "concatenated.pdf"

pdf_merger2.append(conc_file)
pdf_merger2.merge(1,str(m3))

with Path("merged.pdf").open(mode="wb") as output_file:
    pdf_merger2.write(output_file)