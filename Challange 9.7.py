import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
    }

states = []
for key in capitals_dict:
    states.append(key)

print(states)

random_num = random.randint(0,len(states))
rand_state = states[random_num]

user_state = input(f"Enter the capital for {rand_state} state.\n")

while user_state.upper() != capitals_dict[rand_state].upper():
    print("Wrong capital. Try again:")
    user_state = input(f"Enter the capital for {rand_state} state.\n")      
    if user_state.lower() == "exit":
        print("Goodbye")
        break
else:
    print("Correct!")
    
#Refactoring version:
##states = list(capitals_dict.keys())
##
##rand_state = random.choice(states)
##
##while True:
##    user_state = input(f"Enter the capital for {rand_state} state (type 'exit' to quit).\n")
##    
##    if user_state.lower() == "exit":
##        print("Goodbye")
##        break
##    elif user_state.upper() == capitals_dict[rand_state].upper():
##        print("Correct!")
##        break
##    else:
##        print("Wrong capital. Try again:")
