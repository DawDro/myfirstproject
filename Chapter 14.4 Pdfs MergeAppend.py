from PyPDF2 import PdfMerger
from pathlib import Path

pdf_merger = PdfMerger()

reports_dir = (Path.home() / "Desktop" / "Python" / "Python Basics" / "expense_reports")

for path in reports_dir.glob("*.pdf"):
    print(path.name)

expense_reports = list(reports_dir.glob("*.pdf"))
expense_reports.sort()

for path in expense_reports:
    pdf_merger.append(str(path))

with Path("expense_reports.pdf").open(mode="wb") as output_file:
    pdf_merger.write(output_file)


qreports_dir = (Path.home() / "Desktop" / "Python" / "Python Basics" / "quarterly_report")
qrep_path = qreports_dir / "report.pdf"
toc_path = qreports_dir / "toc.pdf"

pdf_merger2 = PdfMerger()

pdf_merger2.append(str(qrep_path))

pdf_merger2.merge(1,str(toc_path))

with Path("full_report.pdf").open(mode="wb") as output_file:
    pdf_merger2.write(output_file)