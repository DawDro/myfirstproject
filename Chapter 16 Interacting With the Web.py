from urllib.request import urlopen


# --------- Scrap page title based on strings
# url = "http://olympus.realpython.org/profiles/aphrodite"
# url = "http://olympus.realpython.org/profiles/poseidon"


# page = urlopen(url)

# html_bytes = page.read()

# html = html_bytes.decode("utf-8")

# print(html)

# title_index = html.find("<title>")
# print(title_index)

# start_index = title_index + len("<title>")
# print(start_index)

# end_index = html.find("</title>")
# print(end_index)

# title = html[start_index:end_index]
# print(title)

# -------------  Based on Regex

import re

# print(re.findall("ab*c","ac",re.IGNORECASE)) # To ignore case sensitivity

# # If no match is found there is empty array returned
# # . for any character, * for anything before it

# print(re.findall("a.*c","abbc"))


# # search 
# match_results = re.search("ab*c","ABC",re.IGNORECASE)
# print(match_results.group())

# # substitute

# string = "Everything is <replaced> if it's in <tags>."
# string = re.sub("<.*>","ELEPHANTS",string)
# print(string)

# # ? matches the shortest text

# string = "Everything is <replaced> if it's in <tags>."
# string = re.sub("<.*?>","ELEPHANTS",string)
# print(string)

# Extracting Text from HTML with regular expression

url = "http://olympus.realpython.org/profiles/dionysus"
page = urlopen(url)
html = page.read().decode("utf-8")

pattern = "<title.*?>.*?</title.*?>"
match_results = re.search(pattern, html, re.IGNORECASE)
title = match_results.group()
title = re.sub("<.*?>", "", title)
print(title)

