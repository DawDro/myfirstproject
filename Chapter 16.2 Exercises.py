# 1. Write a program that grabs the full HTML from the web page at http://olympus.realpython.org/profiles. 
from bs4 import BeautifulSoup
from urllib.request import urlopen

url = "http://olympus.realpython.org/profiles"

page = urlopen(url)
html = page.read().decode("utf-8")
soup = BeautifulSoup(html, "html.parser")
print(soup)


# 2. Using Beautiful Soup, parse out a list of all the links on the page by looking for HTML tags with the name a and retrieving the value taken on by the href attribute of each tag. 

print(soup.find_all("a"))
links = []
for n in soup.find_all("a"):
    links.append(n["href"])

print(links)

# 3. Get the HTML from each of the pages in the list by adding the full path to the filename, and display the text (without HTML tags) on each page using Beautiful Soup’s .get_text() method.

full_links = []
for profile in links:
    full_links.append(profile.replace("/profiles",url))

print(full_links)

for prof in full_links:
    profile_page = urlopen(prof)
    html_prof = profile_page.read().decode("utf-8")
    soup_prof = BeautifulSoup(html_prof, "html.parser")
    print(soup_prof)