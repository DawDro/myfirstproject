import pathlib

# 1 v1
home = pathlib.Path.home() / "Desktop" / "Python" / "Python Basics" / "my_folder" / "my_file.txt"
print(home)

#1 v2
file_path = pathlib.Path(r"C:\Users\dawdr\Desktop\Python\Python Basics\my_folder\my_file.txt")

print(file_path)

# 2
print(file_path.exists())

# 3

print(file_path.name)

# 4
print(file_path.parent.name)
