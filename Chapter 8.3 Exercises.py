#1
print("Ex1 --------")
word = input("Enter a word:")

if len(word) < 5:
    print("Your word is less than 5 characters long")
elif len(word) == 5:
    print("Your word is 5 characters long")
else:
    print("Your word is greater than 5 characters")

#2
print("Ex2 ---------")
number = int(input("I'm thinging of a number between 1 and 10. Giess which one."))

if number == 3:
    print("You win!")
else:
    print("You lose.")
