from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

# Encrypting PDF

pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "newsletter.pdf")

pdf_reader = PdfReader(str(pdf_path))

pdf_writer = PdfWriter()

pdf_writer.append_pages_from_reader(pdf_reader)

pdf_writer.encrypt(user_password="SuperSecret")

output_path = Path.home() / "Desktop" / "Python" / "Python Basics" / "newsletter_protected.pdf"

with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)

# Decrypting PDF
    
pdf_path2 = (Path.home() / "Desktop" / "Python" / "Python Basics" / "newsletter_protected.pdf")

pdf_reader2 = PdfReader(str(pdf_path2))

pdf_reader2.decrypt(password="SuperSecret")

