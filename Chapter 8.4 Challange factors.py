number = int(input("Enter a positive integer:"))

for i in range(1,number):
    if number % i == 0:
        print(f"{i} is a factor of {number}")
