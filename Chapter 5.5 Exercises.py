#1
print("Enter a number")
my_number = input()
my_number = float(my_number)
print(f"{my_number} rounded to 2 decimal places is {round(my_number,2)}")

#2
print("Enter a number")
my_number2 = input()
my_number2 = int(my_number2)
print(f"The absolute value of {my_number2} is {abs(my_number2)}")

#3
print("Enter first number")
my_num = input()
print("Enter second number")
my_num2 = input()
calc = float(my_num) - float(my_num2)
is_int = calc.is_integer()
print(f"The difference between {my_num} and {my_num2} is an integer? {is_int}!")
