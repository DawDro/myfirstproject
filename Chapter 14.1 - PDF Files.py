from pathlib import Path
from PyPDF2 import PdfReader

pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "Pride_and_Prejudice.pdf")
text_file_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "Pride_and_Prejudice.txt" )

pdf = PdfReader(str(pdf_path))

print(len(pdf.pages))
print(pdf.metadata.title)
first_page = pdf.pages[0]
print(first_page.extract_text())

with text_file_path.open(mode="w") as text_file:
    title = pdf.metadata.title
    num_pages = len(pdf.pages)
    text_file.write(f"{title}\nNumber of pages: {num_pages}\n\n")


# Extract text page by page
    for page in pdf.pages:
        text = page.extract_text()
        text_file.write(text)
