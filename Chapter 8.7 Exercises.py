import random

#1

def roll():
    return random.randint(1,6)

#2
rolls_sum = 0

for i in range(10000):
    rolls_sum = rolls_sum + roll()

avg_roll = rolls_sum/10000
print(f"The average number is {avg_roll}")
    
