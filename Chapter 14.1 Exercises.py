from pathlib import Path
from PyPDF2 import PdfReader

pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "zen.pdf")

# 1
pdf = PdfReader(str(pdf_path))

# 2
print(len(pdf.pages))

# 3

print(pdf.pages[0].extract_text())