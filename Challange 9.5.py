import random

nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def random_item(list):
    return random.choice(list)

adj1 = random_item(adjectives)
adj2 = random_item(adjectives)
adj3 = random_item(adjectives)

noun1 = random_item(nouns)
noun2 = random_item(nouns)
noun3 = random_item(nouns)

verb1 = random_item(verbs)
verb2 = random_item(verbs)
verb3 = random_item(verbs)

prep1 = random_item(prepositions)
prep2 = random_item(prepositions)

adv1 = random_item(adverbs)




print("{A/An}" + f"{adj1} {noun1}\n")
print("{A/An}" + f"{adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\
{adv1}, the {noun1} {verb2}\
the {noun2} {verb3} {prep2} a {adj3} {noun3}")
