# 1. Write a program that displays a single button with the default background color and black text that reads "Click me".
# When the user clicks the button, the button background should change to a color randomly selected from the following list:
# ["red", "orange", "yellow", "blue", "green", "indigo", "violet"]

# import tkinter as tk
# import random as r

# window = tk.Tk()


# def color_change():
#     colours = ["red", "orange", "yellow", "blue", "green", "indigo", "violet"]
#     random_color = r.choice(colours)
#     btn_click["text"] = f"{random_color}"
    


# btn_click = tk.Button(master=window, text="Click me!", command=color_change)
# btn_click.pack()

# window.mainloop()


# 2. Write a program that simulates rolling a six-sided die. There should be one button with the text "Roll". 
# When the user clicks the button, a random integer from 1 to 6 should be displayed.

import tkinter as tk
import random as r

window = tk.Tk()
window.rowconfigure([0,1],minsize=35, weight=1)
window.columnconfigure(0,minsize=30, weight=1)

def roll():
    num = r.choice(range(1,7))
    lbl_number["text"] = f"{num}"

btn_roll = tk.Button(master=window, text="Roll!", command=roll)
btn_roll.grid(column=0, row=0,sticky="we")

lbl_number = tk.Label(master=window,text="0")
lbl_number.grid(column=0, row=1, sticky="we")

window.mainloop()