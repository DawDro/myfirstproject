import tkinter as tk

window = tk.Tk()

window.title("Temperature converter")

# window.columnconfigure([0,1,2,3],minsize=30)
# window.rowconfigure(0,minsize=30,pad=20)

def fehrenheit_to_celsius():
    """Convert the value from Fehrenheit to Celcius and insert te result into lbl_result"""
    fehrenheit = ent_temperature.get()
    celsius = (5/9)* (float(fehrenheit) - 32)
    lbl_result["text"] = f"{round(celsius,2)}\N{DEGREE CELSIUS}"

frm_entry = tk.Frame(master=window)
frm_entry.grid(column=0,row=0,padx=10)

ent_temperature = tk.Entry(master=frm_entry, width=10)
ent_temperature.grid(column=0,row=0,sticky="e")

lbl_farhrenheit = tk.Label(master=frm_entry, text="\N{DEGREE FAHRENHEIT}")
lbl_farhrenheit.grid(column=1,row=0, sticky="w")

btn_convert = tk.Button(master=window, text="\N{RIGHTWARDS BLACK ARROW}",command=fehrenheit_to_celsius)
btn_convert.grid(column=1,row=0, pady=10)

lbl_result = tk.Label(master=window, text=f"{100}\N{DEGREE CELSIUS}")
lbl_result.grid(column=2,row=0, padx=10)

window.mainloop()