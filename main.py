#import adder as a
from adder import add, double

# main.py
value = add(2,2)
print(value)

double_value = double(value)
print(double_value)
