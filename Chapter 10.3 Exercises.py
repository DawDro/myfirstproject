class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return f"{self.name} is {self.age} years old"

    def speak(self, sound):
        return f"{self.name} says {sound}"

#1

class GoldenRetrevier(Dog):

    def speak(self, sound="Bark"):
        return f"{self.name} says {sound}" # return super().speak(sound)
    
#2

class Rectangle:
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

class Square(Rectangle):
    def __init__(self,side_length):
        self.side_length = side_length # 1 line for 3 - super().__init__(side_length, side_length)
        self.length = side_length
        self.width = side_length


        
        
