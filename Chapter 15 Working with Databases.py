import sqlite3

connection = sqlite3.connect("test_database.db") # Connect to database or create new while no db with this name

# connection = sqlite3.connect(":memory:") # temporary database only when program works

cursor = connection.cursor() # Cursor is to interact with database - for queres, create tables etc.

query = "SELECT datetime('now','localtime')"

results = cursor.execute(query)

row = results.fetchone() # gets first row of results as tuple
print(row)

time = row[0] # gets first element from "row"
print(time)

connection.close() # close db connection

# As with files using "with" statement automatically opens and closes connection to db
# Do the same as above but with "with"

with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    query = "SELECT datetime('now','localtime')"
    results = cursor.execute(query)
    row = results.fetchone()
    time = row[0]
    print(time)

# Creating table and inserting data into it
    
create_table = """
CREATE TABLE People(
FirstName TEXT,
LastName Text,
Age INT
);"""

insert_values = """
INSERT INTO People VALUES(
'Ron',
'Obvious',
42
);"""

with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute(create_table)
    cursor.execute(insert_values)


# There can be created few statements at once divided by ; as below using executescript:
    sql = """
DROP TABLE IF EXISTS People;
CREATE TABLE People(
    FirstName TEXT,
    LastName TEXT,
    Age INT
);
INSERT INTO People VALUES(
    'Ron',
    'Obvious',
    '42'
);"""

with sqlite3.connect("test_database.db") as connection:
    cursor = connection.cursor()
    cursor.executescript(sql)

# There can be inserted many lines of data at once using executemany as below:

people_values = (
    ("Ron", "Obvious", 42),
    ("Luigi", "Vercotti", 43),
    ("Arthur", "Belling", 28)
)

cursor.executemany("INSERT INTO People VALUES(?, ?, ?)", people_values) # each ? is a representation of column from people_values - this kind of input sould be used to protect by ' errors in input data

cursor.execute(
    "UPDATE People SET Age=? WHERE FirstName=? AND LastName=?;",
    (45, 'Luigi', 'Vercotti')
) # update data with parametrization

# fetchall is for retrieving all data from following statement, fetchone takes first row from output
# cursor.execute(
#         "SELECT FirstName, LastName FROM People WHERE Age > 30;"
#     )
#     for row in cursor.fetchall():
#         print(row)