# 14.7 Challenge: Unscramble a PDF 

# The chapter 14 practice_files folder contains a PDF file called scrambled.pdf that has seven pages. 
# Each page contains a number from 1 through 7, but they are out of order. Additionally, some of the pages are 
# rotated by 90, 180, or 270 degrees in either the clockwise or the counterclockwise direction. Write a program that 
# unscrambles the PDF by sorting the pages according to the number contained in the page text and rotates the page, if needed, 
# so that it’s upright. Note You can assume that every PageObject read from scrambled.pdf has a "/Rotate" key. 
# Save the unscrambled PDF to a file in your home directory called unscrambled.pdf.

from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter, PdfMerger

pdf_path = Path.cwd() / "scrambled.pdf"

pdf_reader = PdfReader(str(pdf_path))

pdf_writer = PdfWriter()

pages_dict = {}

for page in pdf_reader.pages:
    page.rotate(-(page["/Rotate"]))
    page_num = int(page.extract_text())
    pages_dict[page_num] = page

sorted_dict = sorted(pages_dict.items())

for n in sorted_dict:
    pdf_writer.add_page(n[1])


with Path("unscrambled.pdf").open(mode="wb") as  output_file:
    pdf_writer.write(output_file)

