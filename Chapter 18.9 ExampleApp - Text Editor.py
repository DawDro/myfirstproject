import tkinter as tk
from tkinter.filedialog import askopenfilename, asksaveasfilename

# window = tk.Tk()

# window.title("Simple Text Editor")

# window.columnconfigure(1,weight=1 ,minsize=800)
# window.rowconfigure(0, weight=1, minsize=500)

# def open_file():
#     filepath = askopenfilename(
#         filetypes=[("Text Files","*.txt"),("All Files","*.*")]
#     )

#     if not filepath:
#         return
    
#     txt_edit.delete("1.0", tk.END)

#     with open(filepath,"r") as input_file:
#         text = input_file.read()
#         txt_edit.insert(tk.END,text)

#     window.title(f"Simple Text Editor - {filepath}")

# def save_file():

#     filepath = asksaveasfilename(
#         defaultextension="txt",
#         filetypes=[("Text Files","*.txt"),("All Files","*.*")]
#     )

#     if not filepath:
#         return
    
#     with open(filepath, "w") as output_file:
#         text = txt_edit.get("1.0",tk.END)
#         output_file.write(text)

#     window.title(f"Simple Text Editor - {filepath}")


# fr_buttons = tk.Frame(window)
# fr_buttons.grid(row=0, column=0, sticky="ns")

# btn_open = tk.Button(master=fr_buttons, text="Open", command=open_file)
# btn_open.grid(row=0, column= 0, padx=5, pady=5, sticky="ew")

# btn_save = tk.Button(master=fr_buttons, text="Save As...", command=save_file)
# btn_save.grid(row=1, column=0, padx=5, sticky="ew")

# txt_edit = tk.Text(window)
# txt_edit.grid(row=0, column=1, padx=5, pady=5, sticky="nswe")

# window.mainloop()


# Recreate the app

import tkinter as tk

window = tk.Tk()

window.columnconfigure(1, minsize=800, weight=1)
window.rowconfigure(0, minsize=500, weight=1)

def save_file():
    filepath = asksaveasfilename(
        defaultextension=["txt"],
        filetypes= [("Text file","*.txt"),("All files","*.*")]
    )

    if not filepath:
        return 

    with open(filepath,"w") as output_file:
        text = txt_box.get("1.0", tk.END)
        output_file.write(text)

    window.title(f"Simple Text Editor - {filepath}")

def open_file():
    filepath = askopenfilename(
        filetypes= [("Text file","*.txt"),("All files","*.*")]
    )
    
    if not filepath:
        return 
    
    with open(filepath,"r") as input_file:
        text = input_file.read()
        txt_box.insert(tk.END,text)

    window.title(f"Simple Text Editor - {filepath}")


frm = tk.Frame(master=window)
frm.grid(row=0, column=0, sticky="ns")

btn_open = tk.Button(master=frm, text="Open file", command=open_file)
btn_open.grid(column=0, row=0, padx=5, pady=5, sticky="ew")

btn_save = tk.Button(master=frm, text="Save as...", command=save_file)
btn_save.grid(column=0, row=1, padx=5, pady=5, sticky="ew")

txt_box = tk.Text(master=window, padx=5, pady=5)
txt_box.grid(column=1, row=0, padx=5, pady=5, sticky="nswe")

window.mainloop()
