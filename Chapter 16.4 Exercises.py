# 1. Repeat the example in this section to scrape the die roll result, but also include the current time of the quote as obtained from the web page. 
# This time can be taken from part of a string inside a <p> tag that appears shortly after the result of the roll in the web page’s HTML.



import mechanicalsoup

browser = mechanicalsoup.Browser()
page = browser.get("http://olympus.realpython.org/dice")

tag = page.soup.select("#result")[0]
clock = page.soup.select("#time")[0]
result_roll = tag.text
result_clock = clock.text

print(f"The result of your dice roll is: {result_roll} at {result_clock}")
import time

for i in range(4):
    page = browser.get("http://olympus.realpython.org/dice")
    tag = page.soup.select("#result")[0]
    clock = page.soup.select("#time")[0]
    clock_text = clock.text
    result_roll = tag.text
    time_start = clock_text.find(",")
    hour = clock_text[time_start+7:]

    print(f"The result of your dice roll is: {result_roll} at {hour}")
    if i < 3:
        time.sleep(10)