from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter


# 1
pdf_path = Path.cwd() / "top_secret.pdf"

print(pdf_path)

pdf_reader = PdfReader(str(pdf_path))

pdf_writer = PdfWriter()

pdf_writer.append_pages_from_reader(pdf_reader)

pdf_writer.encrypt(user_password="Unguessable")

output_path = Path.cwd() / "top_secret_encrypted.pdf"

with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)


#2
    
pdf_reader2 = PdfReader(str(output_path))

pdf_writer2 = PdfWriter()

pdf_reader2.decrypt(password="Unguessable")

pdf_writer2.append_pages_from_reader(pdf_reader2)

print(pdf_writer2.get_page(0).extract_text())