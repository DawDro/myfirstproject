# 1. Re-create as many of the graphs shown in this section as you can by writing your own programs without referring to the provided code.
# from matplotlib import pyplot as plt

# plt.plot([1,2,3,4,5])
# plt.show()

# xs = [1,2,3,4,5]
# ys = [2,4,5,8,10]

# xs = [1,2,3,4,5]
# ys = [3,-1,4,0,6]




# plt.plot(xs,ys,"g-o") # adds line formatting
# plt.show()

# plt.plot([1, 2, 3, 4, 5],"g-o")
# plt.plot([1, 2, 4, 8, 16],"b-^")
# plt.show()

# fruits = {
#     "apples":10,
#     "oranges":16,
#     "bananas":9,
#     "pears":4,
# }

# print(fruits.keys())
# print(fruits.values())

# plt.bar(fruits.keys(),fruits.values())
# plt.show()

# 2. Do pirates cause global warming? In the chapter 17 practice_files folder is a CSV file with data about the number of pirates and the global temperature. 
# Write a program that visually examines this relationship by reading the pirates.csv file and graphing the number of pirates along the x-axis and the 
# temperature along the y-axis. Add a title and label the graph’s axes, then save the resulting graph as a PNG image file.

from pathlib import Path
import csv
from matplotlib import pyplot as plt

file_path = Path.cwd() / "pirates.csv"

with file_path.open(mode="r",encoding="utf-8") as file:
    reader = csv.DictReader(file)
    pirates_data = []

    for row in reader:
        pirates_data.append(row)

years = []
temps = []
no_pirates = []
for i in pirates_data:
    years.append(i["Year"])
    temps.append(i["Temperature"])
    no_pirates.append(i["Pirates"])



plt.plot(no_pirates,temps)
plt.xlabel("Number of pirates")
plt.ylabel("Temperatures")
plt.title("Pirates according to temperature's change.")
plt.savefig("pirates.png")
plt.show()
