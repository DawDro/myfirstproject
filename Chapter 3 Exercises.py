num = input("Enter a number to be doubled:")
doubled_num = float(num) * 2
print(doubled_num)

num_pancakes = 10
print("I'm going to eat " + str(num_pancakes) + " pancakes")

total_pancakes = 10
eaten_pancakes = 5
print("Only " + str(total_pancakes - eaten_pancakes) + " left.")

#Excersise 4.6
#1
just_string = "10"
int_string = int(just_string)
print(int_string *2)

#2
just_string2 = "10"
float_string = float(just_string2)
print(float_string *2)

#3
my_str = "Hello, hi"
my_int = 5
print(my_str + str(my_int))

#4
print("Give first number:")
first_number = input()
print("Give second number:")
second_number = input()
multiply = float(first_number) * float(second_number)
print("The product of " + str(first_number) + " and " + str(second_number) + " is " + str(multiply))

#Excersise 4.7
#1
weight = float(0.2)
animal = "newt"

print(str(weight)+" is the weight of the " + animal + ".")

#2
print("{} is the weight of the {}.".format(weight,animal))

#3
print(f"{weight} is the weight of the {animal}.")


#Chapter 4.8
phrase = "the surprise is in here somewhere"
print(phrase.find("surprise"))

my_story = "I'm telling you the truth; nothing but the truth!"
print(my_story.replace("the truth","lies"))

#Excercises 4.8
#1
print("AAA".find("a"))

#2
print("Somebody said something to Samantha".replace("s","x"))

#3
inputted_string = input()
print(inputted_string.find("a"))


