# 4.1 Exercises
print('Double "quotation" string')

print("String with apostrophe'")

print("Long string that needs multiple lines\
and does not contain line break.")

print("""Long string that needs multiple lines
and does contain line break.""")

# 4.2 Exercises
check_len = len("Loooooooooong")
print(check_len)

str1 = "First"
str2 = "Second"
full_str = str1 + ", " +str2
print(full_str)

str3 = "bazinga"
print(str3[2:6])

# 4.3 Exercises
str4 = "Animals"
str5 = "Badger"
str6 = "Honey Bee"
str7 = "Honey Badger"

print(str4.lower() +"\n" + str5.lower() +"\n" +str6.lower() +"\n" +str7.lower())
print(str4.upper() +"\n" + str5.upper() +"\n" +str6.upper() +"\n" +str7.upper())

string1 = "   Filet Mignon"
string2 = "Brisket   "
string3 = " Cheeseburger "

print(string1.lstrip() + " " + string2.rstrip() + " " + string3.strip())

string4 = "Becomes"
string5 = "becomes"
string6 = "BEAR"
string7 = "bEautiful"
print(string4.startswith("be"))
print(string5.startswith("be"))
print(string6.startswith("be"))
print(string7.startswith("be"))

print("b" + string4[1:])
print("be" + string6[2:])
print("be" + string7[2:])


