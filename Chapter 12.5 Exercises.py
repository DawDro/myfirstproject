# 1
from pathlib import Path

path = Path.home() / "starships.txt"
path.touch()

starships_list = [
    "Discovery\n",
    "Enterprise\n",
    "Defiant\n",
    "Voyager"
    ]

with path.open(mode="w",encoding="utf-8") as file:
    file.writelines(starships_list)

# 2
with path.open(mode="r",encoding="utf-8") as file:
    for line in file.readlines():
        print(line,end="")

# 3
print("\n\n")
with path.open(mode="r",encoding="utf-8") as file:
    for line in file.readlines():
        if line[0] == "D":
            print(line,end="")
