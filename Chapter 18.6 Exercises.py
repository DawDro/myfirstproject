# 1. Try to re-create all the screenshots in this section without looking at the source code. 
# If you get stuck, check the code and finish your re-creation. 
# Then wait for ten or fifteen minutes and try again.
# Repeat this until you can produce all the screenshots on your own. Focus on the output. 
# It’s okay if your own code is slightly different from the code in the book. 

import tkinter as tk

# ------------ 3 frames one below second with different size and colours
# window = tk.Tk()

# frame1 = tk.Frame(width=100, height=100, bg="red")
# frame2 = tk.Frame(width=50, height=50, bg="yellow")
# frame3 = tk.Frame(width=25, height=25, bg="blue")

# frame1.pack()
# frame2.pack()
# frame3.pack()

# window.mainloop()

# ------------ 3 frames one below one with different height and colours, but dynamic width

# window = tk.Tk()

# frame1 = tk.Frame(height=100, bg="red")
# frame2 = tk.Frame(height=50, bg="yellow")
# frame3 = tk.Frame(height=25, bg="blue")

# frame1.pack(fill=tk.X)
# frame2.pack(fill=tk.X)
# frame3.pack(fill=tk.X)

# window.mainloop()

# ------------ 3 frames one on the right of one with different width and colours, but dynamic height

# window = tk.Tk()

# frame1 = tk.Frame(master=window, width=100, height=100, bg="red")
# frame2 = tk.Frame(master=window, width=50, height=50, bg="yellow")
# frame3 = tk.Frame(master=window, width=25, height=25,bg="blue")

# frame1.pack(fill=tk.Y, side=tk.LEFT)
# frame2.pack(fill=tk.Y, side=tk.LEFT)
# frame3.pack(fill=tk.Y, side=tk.LEFT)

# window.mainloop()

# ----------- 2 labels in frame in window one label in position 0,0 second 75/75 with different colours
# window = tk.Tk()

# frame = tk.Frame(master=window,height=200,width=200,bg="White")
# frame.pack()

# label1 = tk.Label(text="I'm on (0,0)", bg="Red")
# label1.place(x=0,y=0)

# label2 = tk.Label(text="I'm on (75,75)", bg="Blue")
# label2.place(x=75,y=75)

# window.mainloop()

# ------------ 9 labels with borders that shows column and row and relief raised, padding 5 in label and in frame

# window = tk.Tk()

# for i in range(3):
#     for j in range(3):
#         frame = tk.Frame(master=window,borderwidth=1, relief=tk.RAISED)
#         frame.grid(row=i, column=j,padx=5,pady=5)
#         label = tk.Label(master=frame, text=f"Row {i}\nColumn {j}")
#         label.pack(padx=5, pady=5)

# window.mainloop()

# ----------- 2 frames in window with text sticked to different directions

# window = tk.Tk()

# window.columnconfigure(0,minsize=250)
# window.rowconfigure([0,1],minsize=100)


# label1 = tk.Label(text="A")
# label1.grid(row=0,column=0, sticky="ne")

# label2 = tk.Label(text="B")
# label2.grid(row=1,column=0,sticky="sw")


# window.mainloop()

# ------------ 4 labels with different stickys and black background
# window = tk.Tk()

# window.rowconfigure(0,minsize=50)
# window.columnconfigure([0,1,2,3],minsize=50)

# label1 = tk.Label(text="1", background="black",fg="red")
# label1.grid(column=0, row=0)

# label2 = tk.Label(text="2", background="black",fg="red")
# label2.grid(column=1, row=0, sticky="we")

# label3 = tk.Label(text="3", background="black",fg="red")
# label3.grid(column=2, row=0, sticky="ns")

# label4 = tk.Label(text="4", background="black",fg="red")
# label4.grid(column=3, row=0, sticky="wens")

# window.mainloop()


# 2. Below is an image of a window made with Tkinter. Try to re-create the window using the techniques you’ve learned thus far.
# You may use any geometry manager you like.

window = tk.Tk()

window.title("Address Entry Form")

frame_input = tk.Frame(master=window,relief=tk.SUNKEN, borderwidth=4)
frame_input.pack()

lbl_first_name = tk.Label(master=frame_input,text="First Name:")
lbl_first_name.grid(column=0,row=0, sticky="e")
lbl_last_name = tk.Label(master=frame_input,text="Last Name:")
lbl_last_name.grid(column=0,row=1, sticky="e")
lbl_addr1 = tk.Label(master=frame_input,text="Address Line 1:")
lbl_addr1.grid(column=0,row=2, sticky="e")
lbl_addr2 = tk.Label(master=frame_input,text="Address Line 2:")
lbl_addr2.grid(column=0,row=3, sticky="e")
lbl_city = tk.Label(master=frame_input,text="City:")
lbl_city.grid(column=0,row=4, sticky="e")
lbl_state = tk.Label(master=frame_input,text="State:")
lbl_state.grid(column=0,row=5, sticky="e")
lbl_post_code = tk.Label(master=frame_input,text="Postal Code:")
lbl_post_code.grid(column=0,row=6, sticky="e")
lbl_country = tk.Label(master=frame_input,text="Country:")
lbl_country.grid(column=0,row=7, sticky="e")

entry_first_name = tk.Entry(master=frame_input,width=50)
entry_first_name.grid(column=1,row=0)
entry_last_name = tk.Entry(master=frame_input,width=50)
entry_last_name.grid(column=1,row=1)
entry_addr1 = tk.Entry(master=frame_input,width=50)
entry_addr1.grid(column=1,row=2)
entry_addr2 = tk.Entry(master=frame_input,width=50)
entry_addr2.grid(column=1,row=3)
entry_city = tk.Entry(master=frame_input,width=50)
entry_city.grid(column=1,row=4)
entry_state = tk.Entry(master=frame_input,width=50)
entry_state.grid(column=1,row=5)
entry_post_code = tk.Entry(master=frame_input,width=50)
entry_post_code.grid(column=1,row=6)
entry_country = tk.Entry(master=frame_input,width=50)
entry_country.grid(column=1,row=7)

frame_buttons = tk.Frame(master=window)
frame_buttons.pack(side=tk.RIGHT)

btn_clear = tk.Button(master=frame_buttons, text="Clear", width=7, padx=5)
btn_clear.pack(side=tk.LEFT,pady=5, padx=5)

btn_submit = tk.Button(master=frame_buttons, text="Submit", width=7, padx=5)
btn_submit.pack(side=tk.LEFT,pady=5, padx=5)

window.mainloop()