import random

player_points = 0
computer_points = 0
action_list = ("paper", "rock", "scissors")
game_round = 0
rounds_to_win = 3


while player_points < rounds_to_win and computer_points < rounds_to_win:
    game_round = game_round + 1
    print(f"Round: {game_round}")
    player_turn =  input("Choose your action (paper, rock, scissors):\n").lower()
    computer_turn = random.choice(action_list)
    print(f"Computer: {computer_turn}")
    if player_turn == computer_turn.lower():
        print(f"Remis! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "paper" and computer_turn.lower() == "rock":
        player_points += 1
        print(f"Player wins! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "paper" and computer_turn.lower() == "scissors":
        computer_points += 1
        print(f"Computer wins! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "rock" and computer_turn.lower() == "scissors":
        player_points += 1
        print(f"Player wins! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "rock" and computer_turn.lower() == "paper":
        computer_points += 1
        print(f"Computer wins! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "scissors" and computer_turn.lower() == "rock":
        computer_points += 1
        print(f"Computer wins! Player:{player_points} vs Computer:{computer_points}")
    elif player_turn == "scissors" and computer_turn.lower() == "paper":
        player_points += 1
        print(f"Player wins! Player:{player_points} vs Computer:{computer_points}")
    else:
        print("Give correct action")

if player_points > computer_points:
    print(f"Player wins: {player_points} to {computer_points}.")
else:
    print(f"Computer wins: {computer_points} to {player_points}.")
    
    
    
