import tkinter as tk
import easygui
import random
from tkinter.filedialog import asksaveasfilename

word_list = []


def create_list_of_words(word_list):
    """The function takes list of words and creates from it array trimming each word to delete spaces"""
    word_array = word_list.split(",")
    stripped = [word.strip() for word in word_array]
    return stripped

def take_words_to_lists():
    """The function takes each list of words and creates array with it, then checks if there are correct number of words provided and returns lists of words or warning"""
    nouns_list = create_list_of_words(nouns_entry.get())
    verbs_list = create_list_of_words(verbs_entry.get())
    adj_list = create_list_of_words(adjectives_entry.get())
    prep_list = create_list_of_words(prepositions_entry.get())
    adv_list = create_list_of_words(adverbs_entry.get())

    if len(nouns_list) > 2 and len(verbs_list) > 2 and len(adj_list) > 2 and len(prep_list) > 2 and len(adv_list) > 0:
        lists = [nouns_list, verbs_list, adj_list, prep_list, adv_list]
        return generate_potem_from_lists(lists)
    else:
        easygui.msgbox("Please put in at least three nouns, verbs, adjectives, preposition and one adverb to continue", title="Warning!")

def generate_potem_from_lists(list_of_lists):
    nouns = list_of_lists[0]
    verbs = list_of_lists[1]
    adjs = list_of_lists[2]
    preps = list_of_lists[3]
    advs = list_of_lists[4]
    title_adj = random.choice(adjs)

    if title_adj[0] in ["a", "e", "i", "o", "u", "y"]:
        article = "An"
    else:
        article = "A"

    lbl_poem_title["text"] = f"{article} {title_adj} {random.choice(nouns)}"
    lbl_poem_line1["text"] = f"A {random.choice(adjs)} {random.choice(nouns)} {random.choice(verbs)} {preps[0]} the {random.choice(adjs)} {random.choice(nouns)}"
    lbl_poem_line2["text"] = f"{random.choice(advs)}, the {random.choice(nouns)} {random.choice(verbs)}"
    lbl_poem_line3["text"] = f"the {random.choice(nouns)} {random.choice(verbs)} {random.choice(preps)} a {random.choice(adjs)} {random.choice(nouns)}"

def save_file():
    filepath = asksaveasfilename(
        defaultextension=["txt"],
        filetypes= [("Text file","*.txt"),("All files","*.*")]
    )

    if not filepath:
        return 

    with open(filepath,"w") as output_file:
        text = lbl_poem_title["text"] + "\n\n" + lbl_poem_line1["text"] + "\n" + lbl_poem_line2["text"] + "\n" + lbl_poem_line3["text"]
        output_file.write(text)



window = tk.Tk()
window.title("Make your own poem!")

window.columnconfigure([0,1], weight=1)
# window.rowconfigure(0,weight=1, minsize=15)

frm_input = tk.Frame(master=window, height=500)
frm_input.grid(row=1,column=0,sticky="we")

frm_input.columnconfigure([1], minsize=400, weight=1)

instruction_lbl = tk.Label(master=window, text="Enter your favorite words, separated by commas:")
instruction_lbl.grid(row=0, column=0)

nouns_lbl = tk.Label(master=frm_input,text="Nouns:")
nouns_lbl.grid(row=1,column=0, sticky="e")

verbs_lbl = tk.Label(master=frm_input,text="Verbs:")
verbs_lbl.grid(row=2,column=0, sticky="e")

adjectives_lbl = tk.Label(master=frm_input,text="Adjectives:")
adjectives_lbl.grid(row=3,column=0, sticky="e")

prepositions_lbl = tk.Label(master=frm_input,text="Prepositions:")
prepositions_lbl.grid(row=4,column=0, sticky="e")

adverbs_lbl = tk.Label(master=frm_input,text="Adverbs:")
adverbs_lbl.grid(row=5,column=0, sticky="e")

nouns_entry = tk.Entry(master=frm_input)
nouns_entry.grid(row=1,column=1, padx=5, sticky="we")
nouns_entry.insert(0, "programmer,laptop,code") 

verbs_entry = tk.Entry(master=frm_input)
verbs_entry.grid(row=2,column=1, padx=5, sticky="we")
verbs_entry.insert(0, "typed,napped,cheered") 

adjectives_entry = tk.Entry(master=frm_input)
adjectives_entry.grid(row=3,column=1, padx=5, sticky="we")
adjectives_entry.insert(0, "great, smelly, robust") 

prepositions_entry = tk.Entry(master=frm_input)
prepositions_entry.grid(row=4,column=1, padx=5, sticky="we")
prepositions_entry.insert(0, "to,from,on,like") 

adverbs_entry = tk.Entry(master=frm_input)
adverbs_entry.grid(row=5,column=1, padx=5, sticky="we")
adverbs_entry.insert(0, "gracefully")

generate_btn = tk.Button(master=window, text="Generate", command=take_words_to_lists)
generate_btn.grid(row=6, column=0, padx=5, pady=5)

frm_poem = tk.Frame(master=window, height=300, relief=tk.GROOVE, borderwidth=4)
frm_poem.grid(row=7, column=0, sticky="we", padx=5, pady=5)

frm_poem.columnconfigure(0,minsize=200, weight=1)
frm_poem.rowconfigure([9,11],minsize=10)

lbl_your_poem = tk.Label(master=frm_poem, text="Your poem:")
lbl_your_poem.grid(row=8, column=0, sticky="we")

lbl_poem_title = tk.Label(master=frm_poem, text="The title")
lbl_poem_title.grid(row=10, column=0, sticky="we")

lbl_poem_line1 = tk.Label(master=frm_poem, text="The line1")
lbl_poem_line1.grid(row=12, column=0, sticky="we")

lbl_poem_line2 = tk.Label(master=frm_poem, text="The line2")
lbl_poem_line2.grid(row=13, column=0, sticky="we")

lbl_poem_line3 = tk.Label(master=frm_poem, text="The line3")
lbl_poem_line3.grid(row=14, column=0, sticky="we")

btn_save = tk.Button(master=frm_poem,text="Save to file", command=save_file)
btn_save.grid(row=15, column=0, padx=10, pady=10)

window.mainloop()