#1

try:
    integer = int(input("Give your number"))
    print(f"Thanks! Your number is {integer}.")
except ValueError:
    print("It was not integer")

#2

try:
    my_str = input("Please give you word:")
    my_int = int(input("Please number:"))
    print(f"The character under {my_int} index is {my_str[my_int]}.")
except ValueError:
    print("You did not entered the number")
except IndexError:
    print("The word you gave does not have that much characters.")
    
