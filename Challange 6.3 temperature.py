def convert_cel_to_far(celcius):
    f = float(celcius)*9/5+32
    return f

def convert_far_to_cel(far):
    c = (float(far)-32)*5/9
    return c



print("Give temperature in degrees to convert:")
celcius = input()
calculated_celc = convert_cel_to_far(celcius)
print(f"{celcius} degrees is {calculated_celc} in F")

print("Give temperature in F to convert:")
far = input()
calculated_far = convert_far_to_cel(far)
print(f"{calculated_far:.2f} F is {far} in celcius")
