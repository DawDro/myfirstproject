import numpy as np

matrix = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(matrix)

print(matrix[0][1])

# If you create mixed array with numbers and strings then all elements are converted to strings

m2 = matrix * 2
print(m2)

m3 = m2 @ m2 

matrix.flatten() # make one array from all entries
matrix.min() # get lowest entry
matrix.sum() # sum of all elements

# Stacking and shaping arrays
# If their axis sizes match, two arrays can be stacked vertically using np.vstack() or horizontally using np.hstack().

