from matplotlib import pyplot as plt

# plt.plot([1,2,3,4,5])
# plt.show()

# xs = [1,2,3,4,5]
# ys = [2,4,5,8,10]

# xs = [1,2,3,4,5]
# ys = [3,-1,4,0,6]




# plt.plot(xs,ys,"g-o") # adds line formatting
# plt.show()

# ------------ Two lines on one graph

# plt.plot([1, 2, 3, 4, 5],"g-o")
# plt.plot([1, 2, 4, 8, 16],"b-^")
# plt.show()

# ------------ Data from arrays
import numpy as np

# array = np.arange(1,6)
# plt.plot(array)
# plt.show()

# data = np.arange(1,21).reshape(5,4)
# plt.plot(data)
# plt.show()

# ------------ Plot formatting

# days = np.arange(0,21)
# other_site, real_python = days, days**2

# plt.plot(days, other_site)
# plt.plot(days,real_python)

# # Adding formatting
# plt.xticks([0,5,10,15,20])
# plt.xlabel("Days of reading")
# plt.ylabel("Amount of Python Learned")
# plt.title("Python Learned Reading Real Python vs Other Site")
# plt.legend(["Other Site", "Real Python"])
# plt.show()

# ------------------ Bar Charts

# centers = [1, 2, 3, 4, 5]
# tops = [2, 4, 6, 8, 10]

# plt.bar(centers, tops)
# plt.show()

# fruits = {
#     "apples":10,
#     "oranges":16,
#     "bananas":9,
#     "pears":4,
# }

# print(fruits.keys())
# print(fruits.values())

# plt.bar(fruits.keys(),fruits.values())
# plt.show()


# ------------------ Histograms
# from numpy import random

# plt.hist(random.randn(10000), 20)
# plt.show()
