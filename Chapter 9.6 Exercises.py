#1

captains = {}

#2

captains["Enterprise"] = "Picard"
captains["Voyager"] = "Janeway"
captains["Defiant"] = "Sisko"

#3

if "Enterprise" in captains:
    print("")
else:
    captains["Enterprise"] = "unknown"

if "Discovery" in captains:
    print("")
else:
    captains["Discovery"] = "unknown"

#4

for ship in captains:
    print(f"The {ship} is captained by {captains[ship]}.")

#5
del(captains["Discovery"])
print(captains)

#6
captains2 = dict(Enterprise = "Picard",Voyager = "Janeway",Defiant = "Sisko")
print(captains2)
