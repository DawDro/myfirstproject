def multiply(x,y):
    """Return the product of two numbers x and y."""
    product = x*y
    return product

#Exercises 6.2
#1
def cube(x):
    calculate = x**3
    return calculate
#2
def greet(str):
    print(f"Hello, {str}")
