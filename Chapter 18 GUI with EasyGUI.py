import easygui as gui

# Displays msg box with Hello message and button ok "Click me!" to exit window
gui.msgbox(msg="Hello", title="My first message box.",ok_button="Click me!")

# Displays window with choice buttons and takes responsed button
color = gui.buttonbox(
    msg="What is your favorite color?",
    title="Choose color",
    choices=("Red","Yellow","Blue"),
)

print(color)

# As above but returns index on button
color = gui.indexbox(
    msg="What is your favorite color?",
    title="Choose color",
    choices=("Red","Yellow","Blue"), # red = 0, yellow = 1, blue = 2
)

print(color)

# Takes text that user provides

fav_color = gui.enterbox(msg="What is your favorite color?",title="Favorite color")
print(fav_color)

# Takes selected file's path

gui.fileopenbox(title="Choose file:")

# Choose folder instead of file
gui.diropenbox(title="Choose file:")

# Save file path choosed to save file:
gui.filesavebox()

# Exit program while user do not choose anything
path = gui.fileopenbox(title="Select a file")

if path is None: # "is" compares two objects and checks if they are the same 
    exit()

