# Challange 18.3
# Here’s a detailed plan for the application: 
# 1. Ask the user to select a PDF file to open. X
# 2. If no PDF file is chosen, then exit the program. X
# 3. Ask for a starting page number. 
# 4. If the user doesn’t enter a starting page number, then exit the program. 
# 5. Valid page numbers are positive integers. If the user enters an invalid page number:  
#     - Warn the user that the entry is invalid. 
#     - Return to step 3. 
# 6. Ask for an ending page number. 
# 7. If the user doesn’t enter an ending page number, then exit the program. 
# 8. If the user enters an invalid page number:  
#     - Warn the user that the entry is invalid. 
#     - Return to step 6. 
# 9. Ask for the location to save the extracted pages. 
# 10. If the user doesn’t select a save location, then exit the program. 
# 11. If the chosen save location is the same as the input file path:  
#     - Warn the user that they can’t overwrite the input file. 
#     - Return to step 9. 
# 12. Perform the page extraction:  
#     - Open the input PDF file. 
#     - Write a new PDF file containing only the pages in the selected page range.


import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

input_path = gui.fileopenbox(
    "Select a PDF to extract pages.", 
    default="*.pdf"
    )

if input_path is None:
    exit()

# Functions
def get_start_page():
    starting_page = gui.enterbox(
            msg="Enter starting page",
            title="Page number"
        )
    
    if starting_page is None:
        exit()
    elif isinstance(int(starting_page),int) and int(starting_page) >= 0:
        end_page, save_path = get_end_page()
    else:
        gui.msgbox(msg="Please enter correct page number eg. 1, 20, 60...")
        get_start_page()
    
    return starting_page, end_page, save_path
    
def get_end_page():
    ending_page = gui.enterbox(
        msg="Enter ending page",
        title="Page number"
    )
    if ending_page is None:
        exit()
    elif isinstance(int(ending_page), int) and int(ending_page) >= 0:
        end_path = get_save_location()
    else:
        gui.msgbox(msg="Please enter correct page number eg. 1, 20, 60...")
        get_end_page()
    return ending_page, end_path


def get_save_location():
    save_location = gui.filesavebox("Choose where to save files.","Choose destination")
    if save_location is None:
        exit()
    elif save_location == input_path:
        gui.msgbox("File can't be overwritten. Choose another location")
        get_save_location(input_path)
    return save_location

# Program run
start_page, end_page, save_path = get_start_page()
# print(start_page,end_page,save_path)

input_file = PdfReader(input_path)
output_pdf = PdfWriter()

for page in input_file.pages[int(start_page):int(end_page)]:
    output_pdf.add_page(page)

with open(save_path,"wb") as output_file:
    output_pdf.write(output_file)