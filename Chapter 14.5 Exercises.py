# 1

from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter


# pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "split_and_rotate.pdf")

# pdf_reader = PdfReader(str(pdf_path))
# pdf_writer = PdfWriter()


# for page in pdf_reader.pages:
#     page.rotate(-90)
#     pdf_writer.add_page(page)

# with Path("rotated.pdf").open(mode="wb") as output_file:
#     pdf_writer.write(output_file)


# 2
import copy

pdf_path2 = (Path.home() / "Desktop" / "Python" / "Python Basics" / "rotated.pdf")
pdf_reader2 = PdfReader(str(pdf_path2))
pdf_writer2 = PdfWriter()

for page in pdf_reader2.pages:
    left_side = copy.deepcopy(page)

    current_coords = left_side.mediabox.upper_right
    new_coords = (current_coords[0]/2,current_coords[1])

    left_side.mediabox.upper_right = new_coords

    right_side = copy.deepcopy(page)

    right_side.mediabox.upper_left = new_coords

    pdf_writer2.add_page(left_side)
    pdf_writer2.add_page(right_side)

with Path("split.pdf").open(mode="wb") as  output_file:
    pdf_writer2.write(output_file)

