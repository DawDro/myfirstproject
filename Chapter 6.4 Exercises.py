#1
print("Exercise 1")
for n in range(2,11):
    print(n)

#2
print("Exercise 2")

my_int = 2

while my_int <11:
    print(str(my_int))
    my_int += 1

#3
print("Exercise 3")

def doubles(x):
    return int(x) * 2
    
z = 2    
for y in range(0,3):
    z = doubles(z)
    print(z)
    
    
