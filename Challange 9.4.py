import statistics

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(universities_data):
    enrollment_values = []
    tuition_fees = []
    for university in universities_data:
        enrollment_values.append(university[1])
        tuition_fees.append(university[2])
    return enrollment_values, tuition_fees

enrollments, fees = enrollment_stats(universities)
#print(f"Enrollments: {enrollments}")
#print(f"Fees: {fees}")

def median(list):
    return statistics.median(list)

#print(median(enrollments))

def mean(list):
    return statistics.mean(list)

print(f"Total  students: {sum(enrollments):,}")
print(f"Total tuition: $ {sum(fees):,}")
print("")
print(f"Student mean: {mean(enrollments):,.2f}")
print(f"Student median: {median(enrollments):,}")
print("")
print(f"Tuition mean: $ {mean(fees):,.2f}")
print(f"Tuition median: $ {median(fees):,}")
    


    
    
    
