class TheFarm:
    def __init__(self, farm_name, farm_owner, farm_area, farm_ID, has_animals, has_plants, no_of_buildings):
        self.farm_name = farm_name
        self.farm_owner = farm_owner
        self.farm_area = farm_area
        self.farm_ID = farm_ID
        self.has_animals = has_animals
        self.has_plants = has_plants
        self.no_of_buildings = no_of_buildings

    def describe_farm(self):
        return print(f"""Farm name: {self.farm_name}
Farm owner: {self.farm_owner}
Farm area: {self.farm_area} sqm
Farm ID: {self.farm_ID}
Animals on farm? {self.has_animals}
Plants on farm? {self.has_plants}
Buildings on farm: {self.no_of_buildings}""")


class Plants(TheFarm):
    def __init__(self, plant_name, plant_type, plant_area, is_grown):
        self.plant_name = plant_name
        self.plant_type = plant_type
        self.plant_area = plant_area
        self.is_grown = is_grown

    def which_farm(self, farm_name):
        return print(f"The {self.plant_name} is avaiable on the {farm_name}.")

    def is_plant_ready_to_crop(self):
        if (self.is_grown) == True:
            return print(f"The {self.plant_name} is ready to crop.")
        else:
            return print(f"The {self.plant_name} is not grown yet.")
        
class Buildings(TheFarm):
    def __init__(self, building_name, building_area, building_purpose, building_is_empty):
        self.building_name = building_name
        self.building_area = building_area
        self.building_purpose = building_purpose
        self.building_is_empty = building_is_empty

    def is_free(self):
        if self.building_is_empty == True:
            print(f"The {self.building_name} is free.")
        else:
            print(f"The {self.building_name} is occupied.")

class Equipment(Buildings):
    def __init__(self, equipment_name, equipement_type, equipement_service_date):
        self.equipment_name = equipment_name
        self.equipement_type = equipement_type
        self.equipement_service_date = equipement_service_date

    def show_service_date(self):
        return print(f"The service date of {self.equipment_name} is {self.equipement_service_date}.")


