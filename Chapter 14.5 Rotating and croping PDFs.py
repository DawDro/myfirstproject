from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter


# pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "ugly.pdf")

# pdf_reader = PdfReader(str(pdf_path))

# Example 1 --------------------------------------------------------------------
# pdf_writer = PdfWriter()

# for n in range(pdf_reader._get_num_pages()):
#     page = pdf_reader._get_page(n)
#     if n % 2 == 0:
#         page.rotate(90)
#     pdf_writer.add_page(page)    

# with Path("ugly_rotated.pdf").open(mode="wb") as output_file:
#     pdf_writer.write(output_file)

# Example 2 ---------------------------------------------------------------------
# pdf_reader2 = PdfReader(str(pdf_path))
# pdf_writer2 = PdfWriter()

# page = pdf_reader2._get_page(0) # Take page 0

# print(page["/Rotate"]) # Access to rotation information
# page.rotate(90)
# print(page["/Rotate"])

# for page in pdf_reader2.pages:
#     if page["/Rotate"] == -90:
#         page.rotate(90)
#     pdf_writer2.add_page(page)

# with Path("ugly_rotated2.pdf").open(mode="wb") as output_file:
#     pdf_writer2.write(output_file)


# Crop pages ------------------------------------------------------------------------
    
pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "half_and_half.pdf")

pdf_reader = PdfReader(str(pdf_path))


# Horizontal crop
# first_page = pdf_reader._get_page(0)

# print(first_page.mediabox)
# print(first_page.mediabox.lower_left) #tuple outputs
# print(first_page.mediabox.upper_left)
# print(first_page.mediabox.lower_right)
# print(first_page.mediabox.upper_right)
# print(first_page.mediabox.upper_right[0])

# first_page.mediabox.upper_left = (0,480) # Change area of page

# pdf_writer = PdfWriter()
# pdf_writer.add_page(first_page)

# with Path("cropped_page.pdf").open(mode="wb") as  output_file:
#     pdf_writer.write(output_file)

# Vertical crop

first_page = pdf_reader._get_page(0)

import copy

left_side = copy.deepcopy(first_page)

current_coords = left_side.mediabox.upper_right
new_coords = (current_coords[0]/2,current_coords[1])

left_side.mediabox.upper_right = new_coords

right_side = copy.deepcopy(first_page)

right_side.mediabox.upper_left = new_coords

pdf_writer = PdfWriter()

pdf_writer.add_page(left_side)
pdf_writer.add_page(right_side)

with Path("cropped_pages.pdf").open(mode="wb") as  output_file:
    pdf_writer.write(output_file)