# 1. Create a new database with a table named Roster that has three fields: Name, Species, and Age. 
# The Name and Species columns should be text fields, and the Age column should be an integer field.

import sqlite3

create_roster_table = """
DROP TABLE IF EXISTS Roster;
CREATE TABLE Roster(
Name TEXT,
Species Text,
Age INT
);"""

with sqlite3.connect("exercise15_database.db") as connection:
    cursor = connection.cursor()
    cursor.executescript(create_roster_table)
    
# 2 Populate your new table with the following values:
# Name Species Age 
# Benjamin Sisko Human 40 
# Jadzia Dax Trill 300 
# Kira Nerys Bajoran 29 

input_values = (
    ("Benjamin Sisko", "Human", 40),
    ("Jadzia Dax", "Trill", 300),
    ("Kira Nerys", "Bajoran", 29)
)

with sqlite3.connect("exercise15_database.db") as connection:
    cursor = connection.cursor()
    cursor.executemany("INSERT INTO Roster VALUES(?, ?, ?)",input_values)


# 3. Update the Name of Jadzia Dax to be Ezri Dax.
    

with sqlite3.connect("exercise15_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute(
    "UPDATE Roster SET Age=? WHERE Name=? AND Species=?;",
    (300, 'Ezri Dax','Trill'))

# 4. Display the Name and Age of everyone in the table classified as Bajoran.

with sqlite3.connect("exercise15_database.db") as connection:
    cursor = connection.cursor()
    cursor.execute("SELECT Name, Age FROM Roster WHERE Species ='Bajoran';"
    )
    for row in cursor.fetchall():
        print(row)