#1
print("Ex1 --------")
print(1 <= 1) # true
print(1 != 1) # false
print(1 != 2) # true
print("good" != "bad") #true
print("good" != "Good") #true
print(123 == "123") # false

print("Ex2 --------")
#2
print(3 != 4)
print(10 > 5)
print("Jack" != "Jill")
print(42 != "42")
