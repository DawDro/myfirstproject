class Dog:
    # Class attribute
    species = "Canis familiaris"

    # Instance attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age
