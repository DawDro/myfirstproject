import tkinter as tk

# 1. Try to re-create all the screenshots in this section without looking at the source code. 
# If you get stuck, check the code and finish your re-creation. Then wait for ten or fifteen minutes and try again.

# window_lbl = tk.Tk()

# lbl_hello = tk.Label(text="Hello, Tkinter",
#                      fg="white",
#                      bg="black",
#                      height=10,
#                      width=10)

# lbl_hello.pack()

# window_lbl.mainloop()

# window_btn = tk.Tk()

# btn_click_me = tk.Button(text="Click me!",
#                          background="blue",
#                          foreground="yellow",
#                          height=5,
#                          width=20)

# btn_click_me.pack()

# window_btn.mainloop()

# window_entry = tk.Tk()

# lbl_name = tk.Label(text="Name")
# entry_name = tk.Entry()

# lbl_name.pack()

# entry_name.insert(0,"Real Python")
# entry_name.delete(0,1)
# entry_name.pack()
# window_entry.mainloop()

# window_box = tk.Tk()

# text_box = tk.Text()

# text_box.insert("1.0","Hello")
# text_box.insert("2.0","\nWorld")
# text_box.delete("1.0","1.5")
# text_box.pack()

# window_box.mainloop()

# window_frm = tk.Tk()

# frm_1 = tk.Frame()
# frm_1.pack()

# window_frm.mainloop()

# window_frm_a_b = tk.Tk()

# frm_a = tk.Frame()
# frm_b = tk.Frame()

# lbl_frm_a = tk.Label(master=frm_a,text="Frame A")
# lbl_frm_b = tk.Label(master=frm_b,text="Frame B")

# lbl_frm_a.pack()
# lbl_frm_b.pack()
# frm_a.pack()
# frm_b.pack()


# window_frm_a_b.mainloop()

# window_reliefs = tk.Tk()

# border_effects = {
#     "flat": tk.FLAT,
#     "sunken": tk.SUNKEN,
#     "raised": tk.RAISED,
#     "groove": tk.GROOVE,
#     "ridge": tk.RIDGE,
# }

# for relief_name, relief in border_effects.items():
#     frame = tk.Frame(master=window_reliefs,relief=relief,borderwidth=5)
#     frame.pack(side=tk.LEFT)

#     label = tk.Label(master=frame,text=relief_name)
#     label.pack()

# window_reliefs.mainloop()


# 2. Write a program that displays a Button widget that is fifty text units wide and twenty-five text units tall. 
# It should have a white background with blue text that reads "Click here".

# window = tk.Tk()

# btn_clickhere = tk.Button(text="Click here!",
#                           width=50,
#                           height=25,
#                           bg="white",
#                           fg="blue"
#                           )

# btn_clickhere.pack()

# window.mainloop()


# 3. Write a program that displays an Entry widget that is forty text units wide and has a white background and black text. 
# Use .insert() to display text in the Entry widget that reads "What is your name?"

window = tk.Tk()

entry = tk.Entry(width=40,
                 bg="white",
                 fg="black")

entry.insert(0,"What is your name?")
entry.pack()

window.mainloop()

