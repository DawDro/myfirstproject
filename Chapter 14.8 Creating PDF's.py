from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.units import inch, cm # Used to import scales like inch and cm to operate on them
from reportlab.lib.pagesizes import LETTER # Importing module with page sizes

# canvas = Canvas("hello.pdf")

# canvas.drawString(72,72,"Hello World") # Distance from left, distance from botton, text

# canvas.save()

# canvas = Canvas("hello.pdf",pagesize=(612.0,792.0))  # Setting other page size than default
# canvas = Canvas("hello.pdf",pagesize=(8.5 * inch, 11 * inch)) # setting page size with units module
# canvas = Canvas("hello.pdf",pagesize=LETTER) # setting page size with pagesizes module Sizes: A4, LETTER, LEGAL, TABLOID

# --------------------------------------- Setting fonts

# canvas = Canvas("font-example.pdf", pagesize=LETTER)
# canvas.setFont("Times-Roman",18) # Setting font for page
# canvas.drawString(1*inch, 10*inch,"Times New Roman(18pt)")
# canvas.save()

# -------------------------------------- Setting fonts colour
from reportlab.lib.colors import blue

canvas = Canvas("font-colors.pdf", pagesize=LETTER)


canvas.setFont("Times-Roman", 12)

canvas.setFillColor("blue")
canvas.drawString(1*inch, 10*inch,"Blue text")

canvas.save()

# With reportlab, you can create tables, forms, and even high-quality graphics from scratch!


