class Dog:
    species = "Canis familiaris"

    def __init__(self, name, age, coat_color):
        self.name = name
        self.age = age
        self.coat_color = coat_color

    # Instance method
    def __str__(self):
        return f"{self.name} is {self.age} years old"

    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"


#1 Add coat color to class
#2 Create a car class with additional attributes
#3 Add method .drive that adds miles to actual mileage

class Car:
    def __init__(self,color, mileage):
        self.color = color
        self.mileage = mileage

    def __str__(self):
        return f"The {self.color} car has {self.mileage} miles."
    
    def drive(self, miles):
        self.mileage += miles
        
