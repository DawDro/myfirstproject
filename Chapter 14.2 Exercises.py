from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

pdf_path = (Path.home() / "Desktop" / "Python" / "Python Basics" / "Pride_and_Prejudice.pdf")

pdf = PdfReader(str(pdf_path))
pdf_writer = PdfWriter()


# 1
last_page_num = len(pdf.pages)
last_page = pdf._get_page(last_page_num-1)
pdf_writer.add_page(last_page)

with Path("last_page.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)


# 2
for page in range(0,len(pdf.pages)):
    if page % 2 == 0:
        act_page = pdf.pages[page] 
        pdf_writer.add_page(act_page)

with Path("every_other_page.pdf").open(mode="wb") as output_file2: 
    pdf_writer.write(output_file2) 


# 3

for page in pdf.pages[0:150]:
    pdf_writer.add_page(page)

with Path("first_part.pdf").open(mode="wb") as output_file: 
    pdf_writer.write(output_file) 

pdf_writer2 = PdfWriter()

for page2 in pdf.pages[151:last_page_num-1]:
    pdf_writer2.add_page(page2)

with Path("second_part.pdf").open(mode="wb") as output_file2: 
    pdf_writer2.write(output_file2) 