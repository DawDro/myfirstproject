import numpy as np

# 1. Use np.arange() and np.reshape() to create a 3 × 3 NumPy array named A that includes the numbers 3 through 11. 

A = np.arange(3,12)
print(A)

A_reshaped = A.reshape(3,3)
print(A_reshaped)


# 2. Display the minimum, maximum, and mean of all entries in A. 

print(A.min(),A.max(),A.mean())

# 3. Square every entry in A using the ** operator and save the results in an array named B. 

B = 2 ** A
print(B)

# 4. Use np.vstack() to stack A on top of B, then save the results in an array named C. 
C = np.vstack([A,B])
print(C)

# 5. Use the @ operator to calculate the matrix product of C by A. 

print(C @ A)

# 6. Reshape C into an array of dimensions 3 × 3 × 2.
C_reshaped = C.reshape(3,3,2)
print(C_reshaped)