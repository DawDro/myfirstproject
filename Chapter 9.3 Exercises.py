#1

data = ((1,2),(3,4))

#2

counter = 0
for i in data:
    counter += 1
    sum = 0
    for j in i:
        sum += j
    print(f"Row {counter} sum: {sum}")


#3
numbers = [4,3,2,1]

numbers_copy = numbers[:]

print(numbers)
print(numbers_copy)

#4

numbers.sort()
print(numbers)
    

