#1

food = ["rice", "beans"]
print(food)

#2

food.append("broccoli")
print(food)

#3

food.extend(["bread","pizza"])
print(food)

#4

print(food[:2])

#5

print(food[4])

#6

breakfast = "eggs,fruit,orange juice".split(",")
print(breakfast)

#7

print(len(breakfast))

#8

lenghts = [len(item) for item in breakfast]
print(lenghts)
