from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

class PdfFileSplitter:
    def __init__(self,file_name):
        self.file_name = file_name
        self.path = (Path.home() / "Desktop" / "Python" / "Python Basics" / file_name)
        self.pdf = PdfReader(str(self.path))

    def split(self, breakpoint):
        self.writer1 = PdfWriter()
        self.writer2 = PdfWriter()

        for page in self.pdf.pages[0:breakpoint-1]:
            self.writer1.add_page(page)

        for page in self.pdf.pages[breakpoint:]:
            self.writer2.add_page(page)
        

    def write(self,filename):
        with Path(f"{filename}_1.pdf").open(mode="wb") as output_file: 
            self.writer1.write(output_file)
        with Path(f"{filename}_2.pdf").open(mode="wb") as output_file: 
            self.writer2.write(output_file)

p = PdfFileSplitter("Pride_and_Prejudice.pdf")
p.split(3)
p.write("Divided_book")