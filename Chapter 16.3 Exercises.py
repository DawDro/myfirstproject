# 1. Use MechanicalSoup to provide the correct username ("zeus") and password ("ThunderDude") to the login page submission form located at http://olympus.realpython.org/login. 
import mechanicalsoup

import mechanicalsoup

browser = mechanicalsoup.Browser()

url = "http://olympus.realpython.org/login"

login_page = browser.get(url)

# print(page.soup) #show page html

login_html = login_page.soup

form = login_html.html.select("form")[0]
form.select("input")[0]["value"] = "zeus"
form.select("input")[1]["value"] = "ThunderDude"

profiles_page = browser.submit(form, login_page.url)

# 2. Display the title of the current page to determine that you’ve been redirected to the /profiles page. 

print(profiles_page.url)

# 3. Use MechanicalSoup to return to the login page by going back to the previous page.
url = "http://olympus.realpython.org/login"

login_page = browser.get(url)
print(login_page.url)

# Provide an incorrect username and password to the login form, then search the HTML of the returned web 
# page for the text “Wrong username or password!” to determine that the login process failed.
login_page = browser.get(url)

# print(page.soup) #show page html

login_html = login_page.soup

form = login_html.html.select("form")[0]
form.select("input")[0]["value"] = "ze"
form.select("input")[1]["value"] = "Dude"

profiles_page = browser.submit(form, login_page.url)
print(profiles_page.text)

error_text_start = profiles_page.text.find("Wrong")
error_text_finish = profiles_page.text.find("!") +1
print(error_text_start)
print(error_text_finish)
error_text = profiles_page.text[error_text_start:error_text_finish]
print(error_text)