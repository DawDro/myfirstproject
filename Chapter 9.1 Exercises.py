#1

cardinal_numbers = ("first", "second", "third")


#2

print(cardinal_numbers[1])

#3

position1, position2, position3 = cardinal_numbers

print(position1, position2,position3)

#4

my_name = tuple("Dawid")
print(my_name)

#5

print("x" in my_name)

#6

new_tuple = my_name[1:]
print(new_tuple)
