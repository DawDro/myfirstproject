#1
print("Ex1 -----")
print((1<=1) and (1!=1)) # false
print(not(1!=2)) # false
print(("good" != "bad") or False) # true
print(("good" != "Good") and not(1==1)) # false

#2
print("Ex2 -----")
print(False == (not True))
print((True and False) == (True and False))
print(not (True and "A" == "B"))

