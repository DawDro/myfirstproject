# import tkinter as tk

# window = tk.Tk()

# # Label widget -----------------------------------
# greeting = tk.Label(
#     text="GUIs are great!",
#     foreground="white", # fg=
#     background="black",  # bg =
#     width=25, # measured in text units, not pixels etc.
#     height=10 # measured in text units, not pixels etc.
#     )

# greeting.pack()

# # Button widget -----------------------------------
# button = tk.Button(
#     text="Click me!",
#     width=25,
#     height=5,
#     bg="Blue",
#     fg="Yellow",
# )

# button.pack()

# # Entry widget -----------------------------------
# entry = tk.Entry(
#     fg="yellow",
#     bg="blue",
#     width=50
# )

# entry.pack()
# entry.insert(0, "Python")
# entry.insert(0, "Real ")

# # entry_text = entry.get() #Get text from entry box

# # Text Widgets - like entry widget but with multiple line of text -----------------------------------
# text_box = tk.Text()
# text_box.pack()

# # text_box.get("1.0") # Gets the first line first character
# # text_box.get("1.0","1.5") # Gets five characters (from 0 to 5) in first line of text
# # text_box.get("1.0", tk.END) # Gets all text from textbox
# # line breaks "\n" are treated as characters too
# # delete works like get/insert
# # text_box.insert("1.0","Hello") # put Hello at beggining of first line
# # text_box.insert("2.0","\nWorld") # insert text to the second line ( without \n that would be at the end of first line)
# # text_box.insert(tk.END,"\nPut me at the end!") # Add text to the end of box


# window.mainloop()

# Assigning Widgets to Frames -----------------------------------

# import tkinter as tk

# window = tk.Tk()
# frame_a = tk.Frame()
# frame_b = tk.Frame()

# label_a = tk.Label(master=frame_a,text="I'm in frame A.")
# label_a.pack()

# labe_b = tk.Label(master=frame_a,text="I'm in frame B.")
# labe_b.pack()

# # To change order of frames in windows, change packing order below:
# frame_a.pack()
# frame_b.pack()

# window.mainloop()

# Adjusting frame apperance with Reliefs -------------------------
import tkinter as tk

border_effects = {
    "flat":tk.FLAT,
    "sunken":tk.SUNKEN,
    "raised":tk.RAISED,
    "groove":tk.GROOVE,
    "ridge":tk.RIDGE
}

window = tk.Tk()

for relief_name, relief in border_effects.items():
    frame = tk.Frame(master=window,relief=relief,borderwidth=5)

    frame.pack(side=tk.LEFT)

    label = tk.Label(master=frame,text=relief_name)
    label.pack()

window.mainloop()

